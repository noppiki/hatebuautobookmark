import datetime
import json
import logging
import random
import sys
import threading
import time as t
import tkinter
import urllib.parse
from time import time

import feedparser
import requests
from requests_oauthlib import OAuth1

import get_signature_util

logging.basicConfig(filename='system.log', level=logging.WARNING, format="%(asctime)s %(levelname)s %(message)s")


def bookmark(url, token):
    auth = OAuth1(token['consumer_key'], token['consumer_secret'], token['access_token'], token['access_secret'])
    target_url = "http://api.b.hatena.ne.jp/1/my/bookmark"
    response = requests.post(target_url, params={"url": url}, auth=auth)
    if response.status_code == 200:
        print(True)
    else:
        print(False)


def start_hotentry_bookmark(url, threshold, token, duration):
    duration_time = duration.split("-")
    bookmarks = threshold.split("-")
    if "?" in url:
        url = url + "&users="
    else:
        url = url + "?users="
    RSS_URL = url + bookmarks[0]
    hotentry_dic = feedparser.parse(RSS_URL)
    if len(hotentry_dic['entries']) > 0:
        entry = random.choice(hotentry_dic['entries'])
        bookmark_count = entry['hatena_bookmarkcount']
        if int(bookmarks[0]) < int(bookmark_count) < int(bookmarks[1]):
            print(entry['link'])
            bookmark(entry['link'], token)
            t.sleep(random.randint(int(duration_time[0]), int(duration_time[1])))


def get_keywords():
    keywords = {}
    f = open('keyword.txt', "r", encoding="utf-8")
    try:
        line = f.readline()
    except UnicodeDecodeError as e:
        logging.exception(e)

    line = line.replace('\n', '')
    line = line.replace('\r', '')
    keywords.update({line: "keywords"})
    for i in line:
        line = f.readline()
        line = line.replace('\n', '')
        line = line.replace('\r', '')
        keywords.update({line: "keywords"})
    f.close()
    return keywords


def get_hot_entries():
    keywords = {}
    f = open('hotentry.txt', "r", encoding="utf-8")
    try:
        line = f.readline()
    except UnicodeDecodeError as e:
        logging.exception(e)
    line = line.replace('\n', '')
    line = line.replace('\r', '')
    if "?" in line:
        line = line.replace("?", ".rss?")
    else:
        line = line + ".rss"
    keywords.update({line: "hotentry"})
    for l in line:
        line = f.readline()
        line = line.replace('\n', '')
        line = line.replace('\r', '')
        if "?" in line:
            line = line.replace("?", ".rss?")
        else:
            line = line + ".rss"
        keywords.update({line: "hotentry"})
    f.close()
    return keywords


def start_keyword_bookmark(keyword, threshold, token, duration):
    duration_time = duration.split("-")
    bookmarks = threshold.split("-")
    encoded = urllib.parse.quote(keyword)
    feed_url = 'http://b.hatena.ne.jp/search/text?q={}&mode=rss&users={}'.format(encoded, int(bookmarks[0]))
    keyword_dic = feedparser.parse(
        feed_url)
    if len(keyword_dic['entries']) > 0:
        entry = random.choice(keyword_dic['entries'])
        bookmark_count = entry['hatena_bookmarkcount']
        if int(bookmarks[0]) < int(bookmark_count) < int(bookmarks[1]):
            print(entry['link'])
            bookmark(entry['link'], token)
            t.sleep(random.randint(int(duration_time[0]), int(duration_time[1])))


def get_bookmark_list():
    keywords = get_keywords()
    hot_entries = get_hot_entries()
    dicts = [keywords, hot_entries]
    return {k: v for dic in dicts for k, v in dic.items()}


def auto_pilot(token, a):
    not_pilot_times = a["not_pilot"].split("-")
    hour = int(datetime.datetime.now().strftime('%H'))
    from_time = int(not_pilot_times[0])
    to_time = int(not_pilot_times[1])
    if from_time > to_time:
        if to_time <= hour < from_time:
            set_bookmark_target(a, token)
    else:
        if from_time <= hour < to_time:
            pass
        else:
            set_bookmark_target(a, token)

    th_me = threading.Thread(target=auto_pilot, name="th_me",
                             args=(
                                 token, a))
    th_me.daemon = True
    th_me.start()


def set_bookmark_target(a, token):
    bookmark_list = get_bookmark_list()
    bookmark_keys = list(bookmark_list.keys())
    bookmark_target = bookmark_keys[random.randint(0, len(bookmark_keys) - 1)]
    if bookmark_list[bookmark_target] == "keywords":
        start_keyword_bookmark(bookmark_target, a['bookmark'], token, a["yuragi"])
    else:
        start_hotentry_bookmark(bookmark_target, a['bookmark'], token, a["yuragi"])


root = tkinter.Tk()
root.title(u"はてブ自動巡回")
user_id_label = tkinter.Label(text=u'user ID')
user_id_label.pack()
id_entry = tkinter.Entry()


def get_id():
    try:
        account = json.load(open('account.json', 'r'))
        return account['id']
    except FileNotFoundError:
        return ""


def get_password():
    try:
        account = json.load(open('account.json', 'r'))
        return account['password']
    except FileNotFoundError:
        return ""


def save_account(a):
    json.dump(a,
              open('account.json', 'w'))


def create_oauth_header(oauth_params):
    auth_header_entries = []
    for key, value in oauth_params.items():
        auth_header_entries.append("%s=\"%s\"" % (key, value))

    return "OAuth " + ",".join(auth_header_entries)


def get_request_token(consumer_key, consumer_secret, scope):
    nonce = "TekitouNaMojiretsu"
    timestr = str(int(time()))

    request_url = 'https://www.hatena.com/oauth/initiate'

    oauth_params = {
        'realm': '',
        'oauth_callback': 'oob',
        'oauth_consumer_key': consumer_key,
        'oauth_nonce': nonce,
        'oauth_signature': None,
        'oauth_signature_method': 'HMAC-SHA1',
        'oauth_timestamp': timestr,
        'oauth_version': '1.0'
    }
    q_params = {}
    [q_params.update({k: v}) for k, v in oauth_params.items()
     if k.startswith('oauth') if v is not None]
    send_data = {'scope': scope}
    q_params.update(send_data)

    # ハッシュを計算するkeyとdataを生成し、signature計算
    signature = get_signature_util.make_signature(consumer_secret, 'post', request_url, q_params)

    oauth_params["oauth_signature"] = signature

    # request_token取得
    response = requests.post(request_url, headers={"Authorization": create_oauth_header(oauth_params),
                                                   "Content-Type": "application/x-www-form-urlencoded"},
                             params=send_data).text
    # print response
    request_token = response.split("oauth_token=")[1].split("&")[0]
    request_token_secret = response.split("oauth_token_secret=")[1].split("&")[0]

    return {"request_token": urllib.parse.unquote(request_token),
            "request_token_secret": urllib.parse.unquote(request_token_secret)}


def get_rk(hatena_id, password):
    target_url = "https://www.hatena.ne.jp/login"
    payload = {'name': hatena_id, 'password': password}
    response = requests.post(target_url, data=payload)

    try:
        rk = response.headers["Set-Cookie"].split("rk=")[1].split(";")[0]
    except IndexError:
        raise KeyError("cannot get rk using hatena_id: %s and password: %s . ID/Password is wrong, or Hatena API spec "
                       "changed." % (hatena_id, password))
    return rk


def get_verifier_code(request_token, hatena_id, password):
    rk = get_rk(hatena_id, password)
    response = requests.get("https://www.hatena.ne.jp/oauth/authorize?oauth_token=" + urllib.parse.quote(request_token),
                            headers={"Cookie": "rk=" + rk}).text
    rkm = response.split("<input type=\"hidden\" name=\"rkm\" value=\"")[1].split("\"")[0]
    response = requests.post("https://www.hatena.ne.jp/oauth/authorize", headers={"Cookie": "rk=" + rk},
                             params={"rkm": rkm, "oauth_token": request_token,
                                     "name": "%E8%A8%B1%E5%8F%AF%E3%81%99%E3%82%8B"}).text
    verifier = response.split("<div class=verifier><pre>")[1].split("<")[0]

    return verifier


def get_access_token(consumer_secret, request_token, request_token_secret, oauth_verifier_code):
    nonce = "TekitouNaMojidesun"
    timestr = str(int(time()))

    request_url = 'https://www.hatena.com/oauth/token'

    # HeaderのAuthorizationパラメータに含める値
    # oauth_signatureはこれから作成するのでまだなし
    oauth_params = {
        'realm': '',
        'oauth_consumer_key': consumer_key,
        'oauth_nonce': nonce,
        'oauth_signature': None,
        'oauth_signature_method': 'HMAC-SHA1',
        'oauth_timestamp': timestr,
        'oauth_token': request_token,
        'oauth_verifier': oauth_verifier_code,
        'oauth_version': '1.0'
    }

    q_params = {}
    [q_params.update({k: v}) for k, v in oauth_params.items()
     if k.startswith('oauth') if v is not None]

    signature = get_signature_util.make_signature(consumer_secret, 'post', request_url, q_params, request_token_secret)

    oauth_params["oauth_signature"] = signature

    response = requests.post(request_url, headers={"Authorization": create_oauth_header(oauth_params),
                                                   "Content-Type": "application/x-www-form-urlencoded"}).text

    access_token = response.split("oauth_token=")[1].split("&")[0]
    access_token_secret = response.split("oauth_token_secret=")[1].split("&")[0]

    return {"oauth_token": urllib.parse.unquote(access_token),
            "oauth_token_secret": urllib.parse.unquote(access_token_secret)}


def get_token(a):
    global consumer_key
    hatena_id = a['id']
    password = a['password']
    consumer_key = "gjTnWF7N3oJS3g=="
    consumer_secret = "qkAGVq8v65vY19P1wV99hIfl4gQ="
    scope = "read_public,read_private,write_public,write_private"
    request_token_and_secret = get_request_token(consumer_key, consumer_secret, scope)
    request_token = request_token_and_secret["request_token"]
    request_token_secret = request_token_and_secret["request_token_secret"]

    verifier = get_verifier_code(request_token, hatena_id, password)

    access_token_and_secret = get_access_token(consumer_secret, request_token, request_token_secret, verifier)
    access_token = access_token_and_secret["oauth_token"]
    access_secret = access_token_and_secret["oauth_token_secret"]
    return {"consumer_key": consumer_key, "consumer_secret": consumer_secret, "access_token": access_token,
            "access_secret": access_secret}


def start(account):
    global Button
    save_account(account)
    try:
        token = get_token(account)
    except KeyError as e:
        logging.exception(e)
        sys.exit()

    th_me = threading.Thread(target=auto_pilot, name="th_me",
                             args=(token, account))
    th_me.daemon = True
    th_me.start()
    Button.destroy()


def get_bookmark_number():
    try:
        account = json.load(open('account.json', 'r'))
        return account['bookmark']
    except FileNotFoundError:
        return "5-10"


def get_yuragi():
    try:
        account = json.load(open('account.json', 'r'))
        return account['yuragi']
    except FileNotFoundError:
        return "5-10"


def get_not_pilot_time():
    try:
        account = json.load(open('account.json', 'r'))
        return account['not_pilot']
    except FileNotFoundError:
        return "23-1"
    except KeyError:
        return "23-1"


def get_replay_time():
    try:
        account = json.load(open('account.json', 'r'))
        return account['replay']
    except FileNotFoundError:
        return "30"
    except KeyError:
        return "30"


id_entry.insert(tkinter.END, get_id())
id_entry.pack()
password_label = tkinter.Label(text=u'Password')
password_label.pack()
pass_entry = tkinter.Entry()
pass_entry.insert(tkinter.END, get_password())
pass_entry.pack()
bookmark_label = tkinter.Label(text=u'被ブックマーク数範囲')
bookmark_label.pack()
bookmark_entry = tkinter.Entry()
bookmark_entry.insert(tkinter.END, get_bookmark_number())
bookmark_entry.pack()

yuragi_label = tkinter.Label(text=u'ブックマーク間隔（秒）')
yuragi_label.pack()

yuragi = tkinter.Entry()
yuragi.insert(tkinter.END, get_yuragi())
yuragi.pack()

not_pilot_label = tkinter.Label(text=u'巡回しない時間（24時間で）')
not_pilot_label.pack()

not_pilot = tkinter.Entry()
not_pilot.insert(tkinter.END, get_not_pilot_time())
not_pilot.pack()

Button = tkinter.Button(text=u'開始', command=lambda: start(
    {"id": id_entry.get(), "password": pass_entry.get(), "bookmark": bookmark_entry.get(),
     "yuragi": yuragi.get(), "not_pilot": not_pilot.get()}))
Button.pack()
Button2 = tkinter.Button(text=u'終了', command=lambda: sys.exit())
Button2.pack()
root.mainloop()
