#!/usr/bin/python
# -*- coding:utf-8 -*-
import urllib


def normalize_key(consumer_secret, oauth_token_secret=None):
    if oauth_token_secret is None:
        key = urllib.parse.quote(consumer_secret, '') + "&"
    else:
        key = "%s&%s" % (urllib.parse.quote(consumer_secret, ''), urllib.parse.quote(oauth_token_secret, ''))

    return key


def normalize_data(http_method, request_url, header_params):
    query_str = '&'.join([
        '='.join(
            [key, urllib.parse.quote(header_params.get(key, ''), '')]
        ) for key in sorted(header_params.keys())
    ])

    target_str = '&'.join(
        [urllib.parse.quote(val, '')
         for val in [http_method.upper(), request_url.lower(), query_str]
         if val is not None])

    return target_str


def make_signature(consumer_secret, http_method, request_url, header_params, oauth_token_secret=None):
    import hashlib
    import hmac
    import base64

    normalized_key = normalize_key(consumer_secret, oauth_token_secret)
    normalized_data = normalize_data(http_method, request_url, header_params)

    signature = hmac.new(
        normalized_key.encode('utf-8'),
        normalized_data.encode('utf-8'),
        hashlib.sha1
    )
    return base64.b64encode(signature.digest()).decode('utf-8')
